from django.db import models, IntegrityError
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.core.validators import validate_email


# Create your models here.

class Uploader(models.Model):
    class Meta:
        db_table = 'uploader'

    name = models.CharField(max_length=100)
    file = models.FileField(upload_to="media/files")

    def __str__(self) -> str:
        return self.name


class Cashier(AbstractUser):
    class Meta:
        db_table = 'cashier'

    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=50, null=False, blank=False)
    cc = models.CharField(max_length=15, unique=True, null=False, blank=False)
    email = models.EmailField()
    password1 = models.CharField(max_length=30, null=False, blank=False)
    password2 = models.CharField(max_length=30, null=False, blank=False)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Client(models.Model):
    class Meta:
        db_table = 'client'

    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=50, null=False, blank=False)
    cc = models.CharField(max_length=15, unique=True, null=False, blank=False)
    email = models.EmailField(unique=True, null=True, blank=True,
                              validators=[
                                  validate_email
                              ])

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def save(self, *args, **kwargs):
        try:
            validate_email(self.email)
        except ValidationError as error:
            raise IntegrityError(error.message)
        super().save(*args, **kwargs)


class Barber(models.Model):
    class Meta:
        db_table = 'barber'

    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=50, null=False, blank=False)
    cc = models.CharField(max_length=15, unique=True, null=False, blank=False)
    email = models.EmailField()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Turn(models.Model):
    TYPE = (
        ('S', 'simple'),
        ('BC', 'beard cutting'),
        ('LC', 'line cut'),
        ('BLC', 'beard and line cutting')
    )

    class Meta:
        db_table = 'turn'

    number = models.CharField(max_length=5)
    type = models.CharField(max_length=3, choices=TYPE)
    price = models.FloatField()
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
    id_barber = models.ForeignKey(Barber, on_delete=models.CASCADE)
    id_cashier = models.ForeignKey(Cashier, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.number} {self.type} {self.price} {self.id_barber}'
