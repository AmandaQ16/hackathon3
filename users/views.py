from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView, LogoutView
import pathlib
from django.urls import reverse_lazy
from utils.utils import Utils
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)
from users.models import Uploader, Turn, Client
from users.forms import CreateUserForm


def home(request):
    return render(request, 'base.html')


class CustomLoginView(LoginView):
    template_name = 'registration/login.html'
    success_url = reverse_lazy('')


def register_user(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = CreateUserForm()
    context = {'form': form}
    return render(request, 'registration/register.html', context)

def upload_files(request):
    valid_ext = ".xlsx"
    message = None
    document = None
    if request.method == "POST" and request.FILES["file"]:
        name = request.POST.get("name", "")
        file = request.FILES.get("file", "")
        extension = pathlib.Path(file.name).suffix
        if str(extension) in valid_ext:
            document = Uploader(name=name, file=file)
            document.save()
            message = None
            return redirect(f"/---------/{document.id}")
        else:
            message = "Archivo no Valido"
    return render(request, '/---------/', {"message": message})


def read_file(request, id):
    file = Uploader.objects.get(pk=id)
    extension = pathlib.Path(file.file.path).suffix
    if extension == ".xlsx":
        return redirect(f"/lector/save_csv/{file.id}")


def save_file_turn(request):
    file = Uploader.objects.get(pk=id)
    date = Utils.reader_excel(file.file.path)

    for line in date:
        Turn.objects.create(
            number=int(line['number']),
            type=line['type'],
            price=float(line['price']),
            id_client=int(line['id_client']),
            id_barber=int(line['id_barber']),
            id_cashier=int(line['id_cashier'])
        )
    redirect('/-------/')


def save_file_client(request):
    file = Uploader.objects.get(pk=id)
    date = Utils.reader_excel(file.file.path)

    for line in date:
        Client.objects.create(
            first_name=line['first_name'],
            last_name=line['last_name'],
            cc=line['cc'],
            email=line['email'],
        )
    redirect('/-----------/')
