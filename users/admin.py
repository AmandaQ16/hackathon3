from django.contrib import admin

# Register your models here.
from users.models import Cashier, Client, Barber, Turn


@admin.register(Cashier)
class CashierAdmin(admin.ModelAdmin):
    search_fields = (
        'first_name',
        'cc',
    )


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    search_fields = (
        'first_name',
        'cc',
    )


@admin.register(Barber)
class BarberAdmin(admin.ModelAdmin):
    search_fields = (
        'first_name',
        'cc',
    )


@admin.register(Turn)
class TurnAdmin(admin.ModelAdmin):
    search_fields = (
        'turn',
    )
