from django.urls import path, include

from users.views import home, CustomLoginView, register_user

urlpatterns = [path('', home, name="home"),
               path('login/', CustomLoginView.as_view(), name='login'),
               path('register/', register_user, name='register'),
               path('', include('django.contrib.auth.urls')),
               ]
