import csv
import pandas as panda


class Utils:
    def reader_csv(path_file):
        data_list = []
        with open('path_file') as file:
            data = csv.DictReader(file)
            for line in data:
                data_list.append(line)

        return data_list

    def reader_excel(path):
        df = panda.read_excel(io=path)
        return df
